import setupApp from './src/app';
import * as functions from 'firebase-functions';

setupApp().listen(process.env.PORT || 3000, () => {
  console.log('RODANDO NA PORTA 3000')
})