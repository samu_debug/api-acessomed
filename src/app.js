import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import routes from './routes';

import * as admin from 'firebase-admin';

const credentials = require('./config/acesso-med-firebase-adminsdk-zr8zp-d5b8557f9c.json')


const app = express();

const configureExpress = () => {
  app.use(cors({origin: '*'}));
  app.use(bodyParser.json());
  if(admin.apps.length == 0){
  admin.initializeApp(credentials);
  }
  app.use('/', routes);
  return app;
};

export default configureExpress;
