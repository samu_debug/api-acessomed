import User from '../models/user';
import Hash from '../util/hash';
class AgenteController {
  constructor(Agente) {
    this.Hash = new Hash();
    this.Agente = Agente;
  }

  isEmptyObject(obj) {
    return (Object.getOwnPropertyNames(obj).length === 0);
  }

  checkAdmin(req, res) {
    try {
      if (req.currentUser.type !== 'admin') {
        res.status(401);
      }
    } catch (err) {
      res.status(401);
    }

  }
  async get(req, res) {
    this.checkAdmin(req, res);
    try {
      let agentes = await this.Agente.get();
      let data = [];
      if (typeof req.query.situacao !== 'undefined') {
        
        if (req.query.situacao === 'ativo') {
          await agentes.forEach((doc) => {
            if (doc.data().isActive) {
              const retorno = doc.data();
              retorno.id = doc.id;
              data.push(retorno);
            }
          });


        } else if (req.query.situacao === 'inativo') {
          await agentes.forEach((doc) => {

            if (!(doc.data().isActive)) {
              const retorno = doc.data();
              retorno.id = doc.id;
              data.push(retorno);
            }
          });
        }
      } else {
        await agentes.forEach((doc) => {
          const agente = doc.data();
          agente.id = doc.id;
          data.push(agente);
        });
      }
      res.send(data);
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  }

  async getById(req, res) {
    try {
      const agente = await this.Agente.doc(req.params.id).get();
      if (agente.exists) {
        if (req.currentUser.type === 'agente' && req.currentUser.cpf === agente.data().cpf || req.currentUser.type === 'admin') {
          const retorno = agente.data();
          retorno.id = agente.id;
          res.send(retorno);
        } else {
          res.sendStatus(401);
        }

      } else {
        res.status(404).json({ message: 'Este agente não existe' });
      }
    } catch (err) {
      res.status(417).json({ message: 'ERRO!' });
    }
  }

  async create(req, res) {
    this.checkAdmin(req, res);
    try {
      const agente = await this.Agente.where('cpf', '==', req.body.cpf).get();
      if (!agente.empty) {
        res.status(409).json({ message: 'Este Agente já existe' });
      } else {
        const user = await User.where('username', '==', req.body.cpf).limit(1).get();
        if (user.empty) {
          req.body.password = await this.Hash.hash(req.body.password);
          await User.add({
            username: req.body.cpf,
            name: req.body.name,
            password: req.body.password,
            type: 'agente',
            isActive: true
          });
          const newAgente = await this.Agente.add({
            cpf: req.body.cpf,
            documentos: '',
            email: req.body.email,
            endereco: req.body.endereco,
            estado: req.body.estado,
            cidade: req.body.cidade,
            foto_perfil: req.body.foto_perfil,
            name: req.body.name,
            rg: req.body.rg,
            telefone: req.body.telefone,
            isActive: true
          });
          const newAgenteData = await newAgente.get();
          const retorno = newAgenteData.data();
          retorno.id = agente.id;
          res.send(retorno);
        } else {
          res.status(409).json({ message: 'Este Agente já existe' })
        }
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ message: "Erro!" });
    }

  }

  async update(req, res) {
    try {

      const agente = await this.Agente.doc(req.params.id);
      const agenteRef = await agente.get()
      if (agenteRef.exists) {

        let userDoc = null;
        if (req.body.hasOwnProperty('cpf') && req.body.cpf != '') {
          const userQuery = await User.where('username', '==', agenteRef.data().cpf).limit(1).get();
          userDoc = await User.doc(userQuery.docs[0].id);
          userDoc.update({ username: req.body.cpf });
        }
        if (req.body.hasOwnProperty('name') && req.body.name != '') {
          const userQuery = await User.where('username', '==', agenteRef.data().cpf).limit(1).get();
          userDoc = await User.doc(userQuery.docs[0].id);
          userDoc.update({ name: req.body.name })
        }

        if (req.body.hasOwnProperty('password') && req.body.password != '') {
          const userQuery = await User.where('username', '==', agenteRef.data().cpf).limit(1).get();
          userDoc = await User.doc(userQuery.docs[0].id);
          const newPassword = await this.Hash.hash(req.body.password);
          userDoc.update({ password: newPassword });
        }
        if (req.body.hasOwnProperty('isActive') && req.body.isActive != null) {
          const userQuery = await User.where('username', '==', agenteRef.data().cpf).limit(1).get();
          userDoc = await User.doc(userQuery.docs[0].id);
          userDoc.update({ isActive: req.body.isActive });
        }
        if (req.body.hasOwnProperty('cpf') && req.body.cpf == '') {
          delete req.body.cpf;
        }
        if (req.body.hasOwnProperty('email') && req.body.email == '') {
          delete req.body.email;
        }
        if (req.body.hasOwnProperty('endereco') && req.body.endereco == '') {
          delete req.body.endereco;
        }
        if (req.body.hasOwnProperty('isActive') && req.body.isActive == null) {
          delete req.body.isActive;
        }
        if (req.body.hasOwnProperty('name') && req.body.name == '') {
          delete req.body.name;
        }
        if (req.body.hasOwnProperty('rg') && req.body.rg == '') {
          delete req.body.rg;
        }
        if (req.body.hasOwnProperty('telefone') && req.body.telefone == '') {
          delete req.body.telefone;
        }
        if (Object.keys(req.body).length > 0) {
          await agente.update(req.body);
        }
        const updated = await agente.get();
        res.send(updated.data());
      } else {
        res.sendStatus(401);
      }
    } catch (err) {
      console.log(err);
      res.sendStatus(417);
    }


  }

  async delete(req, res) {
    this.checkAdmin(req, res);
    try {
      const doc = await this.Agente.doc(req.params.id);
      const agenteRef = await doc.get();
      const userQuery = await User.where('username', '==', agenteRef.data().cpf).limit(1).get();
      if (!userQuery.empty) {
        await User.doc(userQuery.docs[0].id).delete();
      }
      await doc.delete();
      res.send({ message: "Sucesso" });
    } catch (err) {
      console.log(err);
      res.status(417).json({ message: "Erro!" });
    }
  }
}
export default AgenteController;