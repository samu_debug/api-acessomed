import * as firebase from 'firebase-admin';
import Hash from '../util/hash';

class AuthController {
    constructor(User) {
        this.Hash = new Hash();
        this.User = User;
    }

    async login(req, res) {
        try {
          let user = await this.User.where('username', '==', req.body.username).limit(1).get();

          if (user.empty) {
            res.status(401).json({message: 'Username incorreto'});
          }else{
            if(await this.Hash.compare(req.body.password, user.docs[0].data().password)){
              const options = {
                type: user.docs[0].data().type,
                id: user.docs[0].id
              }
              const token = await firebase.auth().createCustomToken(req.body.username, options);
              res.send({token: token, type: options.type, username:req.body.username});
            }else{
              res.status(401).json({message: 'Senha incorreta'});
            }
            }
          } catch (err){
            console.log(err);
            res.sendStatus(500);
        }
      } 

    async check(req, res){
        try {
            const token = req.header('Token');
            const decodedToken = await firebase.auth().verifyIdToken(token);
            req.userId = decodedToken.id;
            req.userType = decodedToken.type;
            res.send({message:'Ok', type: decodedToken.type});
        }catch(err) {
          console.log(err);
          res.status(401).json({message: 'Token inválido'});
        }
        
    }
}

export default AuthController;