class ClienteController {
    constructor(Cliente) {
        this.Cliente = Cliente;
    }

    async get(req, res) {
        try {
            const clientes = await this.Cliente.get();
            let data = [];
            if (typeof req.query.situacao !== 'undefined') {
                if (req.query.situacao == 'ativo') {
                    await clientes.forEach((doc) => {
                        if (doc.data().isActive) {
                            const cliente = doc.data();
                            cliente.id = doc.id;
                            data.push(cliente);
                        }
                    });
                } else if (req.query.situacao == 'inativo') {
                    await clientes.forEach((doc) => {
                        if (!(doc.data().isActive)) {
                            const cliente = doc.data();
                            cliente.id = doc.id;
                            data.push(cliente);
                        }
                    });
                }
            } else {
                await clientes.forEach((doc) => {
                    const cliente = doc.data();
                    cliente.id = doc.id;
                    data.push(cliente);
                });
            }
            res.send(data);
        } catch (err) {
            console.log(err);
            res.sendStatus(500);
        }
    }

    async getById(req, res) {
        try {
            const cliente = await this.Cliente.doc(req.params.id).get();
            if (cliente.exists) {
                const retorno = cliente.data()
                retorno.id = cliente.id;
                res.send(retorno);
            } else {
                res.status(404).json({ message: 'Este cliente não existe' });
            }
        } catch (err) {
            console.log(err);
            res.sendStatus(500);
        }
    }

    async create(req, res) {
        try {
            const cliente = await this.Cliente.where('cpf', '==', req.body.cpf).get();
            if (!cliente.empty) {
                res.status(409).json({ message: 'Este cliente já existe' });
            } else {
                const newCliente = await this.Cliente.add({
                    cpf: req.body.cpf,
                    data_nascimento: req.body.dataNascimento,
                    endereco: req.body.endereco,
                    nome: req.body.nome,
                    responsavel: req.body.responsavel,
                    rg: req.body.rg,
                    estado: req.body.estado,
                    cidade: req.body.cidade
                });
                const newClienteData = await newCliente.get();
                const retorno = newClienteData.data();
                retorno.id = newClienteData.id;
                res.send(retorno);
            }
        } catch (err) {
            console.log(err);
            res.sendStatus(500);
        }
    }

    async update(req, res) {
        try {
            const cliente = await this.Cliente.doc(req.params.id);
            const clienteRef = await cliente.get();
            if (clienteRef.exists) {
                if (req.body.hasOwnProperty('cpf') && req.body.cpf == '') {
                    delete req.body.cpf;
                }
                if (req.body.hasOwnProperty('dataNascimento') && req.body.email == '') {
                    delete req.body.email;
                }
                if (req.body.hasOwnProperty('endereco') && req.body.endereco == '') {
                    delete req.body.endereco;
                }
                if (req.body.hasOwnProperty('rg') && req.body.rg == '') {
                    delete req.body.rg;
                }
                if (Object.keys(req.body).length > 0) {
                    await cliente.update(req.body);
                }
                const updated = await cliente.get();
                res.send(updated.data());
            } else {
                res.sendStatus(401);
            }
        } catch (err) {
            console.log(err);
            res.sendStatus(500);
        }
    }

    async delete(req, res) {
        try {
            const doc = await this.Cliente.doc(req.params.id);
            await doc.delete();
            res.send({ message: 'Sucesso' });
        } catch (err) {
            console.log(err);
            res.sendStatus(500);
        }
    }
}

export default ClienteController;