import Hash from '../util/hash';
import User from '../models/user';
import * as firebase from 'firebase-admin';

class ClinicasController {
    constructor(Clinica) {
        this.Clinica = Clinica;
        this.Hash = new Hash();
    }

    checkAdmin(req, res) {
        try {
            if (req.currentUser.type !== 'admin') {

                res.status(401)
            }
        } catch (err) {
            console.log(err);
            res.status(401)
        }
    }

    async get(req, res) {
        // this.checkAdmin(req, res);
        try {
            const clinicas = await this.Clinica.get();
            let data = [];
            if (typeof req.query.situacao !== 'undefined') {
                if (req.query.situacao === 'ativo') {
                    await clinicas.forEach((doc) => {
                        if (doc.data().isActive) {
                            const clinica = doc.data();
                            clinica.id = doc.id;
                            data.push(clinica);
                        }
                    });
                } else if (req.query.situacao === 'inativo') {
                    await clinicas.forEach((doc) => {
                        if (!(doc.data().isActive)) {
                            const clinica = doc.data();
                            clinica.id = doc.id;
                            data.push(clinica);
                        }
                    })
                }
            }
            res.send(data);
        } catch (err) {
            console.log(err);
            res.status(500).json({ message: 'Erro!' });
        }
    }

    async getById(req, res) {
        // this.checkAdmin(req, res);
        try {
            const clinica = await this.Clinica.doc(req.params.id).get();
            if (clinica.exists) {
                let retorno = clinica.data();
                retorno.id = clinica.id;
                res.send(retorno);
            } else {
                res.status(404).json({ message: 'Esta clínica não existe' });
            }

        } catch (err) {
            console.log(err);
            res.status(500).json({ message: 'Erro!' });
        }
    }

    async create(req, res) {
        this.checkAdmin(req, res);
        try {
            const clinica = await this.Clinica.where('cnpj', '==', req.body.cnpj).limit(1).get();
            if (!clinica.empty) {
                res.status(409).json({ 'message': 'Esta clínica já existe!' });
            } else {
                const user = await User.where('username', '==', req.body.cnpj).limit(1).get();
                if (user.empty) {
                    req.body.password = await this.Hash.hash(req.body.password);
                    await User.add({
                        username: req.body.cnpj,
                        password: req.body.password,
                        name: req.body.name,
                        type: 'clinica',
                        isActive: true,
                    });

                    const newClinica = await this.Clinica.add({
                        cnpj: req.body.cnpj,
                        endereco: req.body.endereco,
                        estado: req.body.estado,
                        cidade: req.body.cidade,
                        name: req.body.name,
                        isActive: true,
                        procedimentos: [],
                        especialidades: req.body.especialidades
                    });
                    const newClinicaRef = await newClinica.get();
                    const retorno = newClinicaRef.data();
                    retorno.id = newClinicaRef.id;
                    res.send(retorno);
                }
            }

        } catch (err) {
            console.log(err);
            res.status(500).json({ message: 'Erro!' });
        }
    }

    async update(req, res) {
        this.checkAdmin(req, res);
        try {
            const clinica = await this.Clinica.doc(req.params.id);
            const clinicaRef = await clinica.get();
            if (clinicaRef.exists) {
                let userDoc;
                if (req.body.hasOwnProperty('cnpj') && req.body.cnpj != '') {

                    const userQuery = await User.where('username', '==', clinicaRef.data().cnpj).limit(1).get();
                    if (!userQuery.empty) {
                        userDoc = await User.doc(userQuery.docs[0].id);
                        userDoc.update({ username: req.body.cnpj });
                    }

                }
                if (req.body.hasOwnProperty('name') && req.body.name != '') {
                    const userQuery = await User.where('username', '==', clinicaRef.data().cnpj).limit(1).get();
                    if (!userQuery.empty) {
                        userDoc = await User.doc(userQuery.docs[0].id);
                        userDoc.update({ name: req.body.name });
                    }

                }
                if (req.body.hasOwnProperty('password') && req.body.password != '') {
                    
                    const userQuery = await User.where('username', '==', clinicaRef.data().cnpj).limit(1).get();
                    if (!userQuery.empty) {
                        userDoc = await User.doc(userQuery.docs[0].id);
                        this.Hash.hash(req.body.password).then((newPassword) => {
                            userDoc.update({ password: newPassword });
                            delete req.body.password;
                        });
                        
                    }
                    userDoc = await User.doc(userQuery.docs[0].id);
                    const newPassword = await this.Hash.hash(req.body.password);
                    userDoc.update({ password: newPassword });
                    delete req.body.password;
                }
                if (req.body.hasOwnProperty('cnpj') && req.body.cnpj == '') {
                    delete req.body.cnpj;
                }
                if (req.body.hasOwnProperty('name') && req.body.name == '') {
                    delete req.body.name;
                }
                if (req.body.hasOwnProperty('endereco') && req.body.endereco == '') {
                    delete req.body.endereco;
                }
                if (Object.keys(req.body).length > 0) {
                    await clinica.update(req.body);
                }

                const updated = await clinica.get();
                res.send(updated.data());
            } else {
                res.status(404).json({ message: 'Esta clínica não existe!' });
            }
        } catch (err) {
            console.log(err);
            res.status(500).json({ message: 'Erro!' });
        }
    }

    async delete(req, res) {
        this.checkAdmin(req, res);
        try {
            const doc = await this.Clinica.doc(req.params.id);
            const docRef = await doc.get();
            if (docRef.exists) {
                const userQuery = await User.where('username', '==', docRef.data().cnpj).limit(1).get();
                if (!userQuery.empty) {
                    await User.doc(userQuery.docs[0].id).delete();
                } else {
                    res.status(404).json({ message: 'Esta clínica não existe' });
                }
                doc.delete();
                res.send({ message: 'Sucesso' });
            } else {
                res.status(404).json({ message: 'Esta clínica não existe' });
            }
        } catch (err) {
            console.log(err);
            res.status(500).json({ message: 'Erro' });
        }
    }

    async createProcedimento(req, res) {
        // this.checkAdmin(req, res);
        try {
            const doc = await this.Clinica.doc(req.params.id);
            await doc.update({
                procedimentos: firebase.firestore.FieldValue.arrayUnion(req.body)
            });
            const docRef = await doc.get();
            const docData = await docRef.data();
            res.send(docData);
        } catch (err) {
            console.log(err);
            res.status(500).json({ message: 'Erro' });
        }
    }
}

export default ClinicasController;