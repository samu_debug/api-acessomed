import * as firebase from "firebase-admin";
import Cliente from "../models/cliente";
import Clinica from "../models/clinica";

class ComprasController {
  constructor(Compras) {
    this.Compras = Compras;
  }

  _formatCompra(docReference) {
    let compra = {};
    compra.id = docReference.id;
    compra.cliente = docReference.data().cliente.id;
    compra.clinica = docReference.data().clinica.id;
    compra.procedimento = docReference.data().procedimento;
    compra.pago = docReference.data().pago;
    const data = docReference.data().data_compra.toDate();
    compra.dataCompra = `${data.getDate()}/${data.getMonth() +
      1}/${data.getFullYear()}`;
    return compra;
  }

  async _formatFullCompra(docReference) {
    let compra = this._formatCompra(docReference);
    compra = await this._formatCliente(compra);
    compra = await this._formatClinica(compra);
    return compra;
    
  }

  async _formatCliente(compra) {
    let cliente = await Cliente.doc(compra.cliente).get();
    compra.cliente = cliente.data();
    return compra;
  } 

  async _formatClinica(compra) {
    let clinica = await Clinica.doc(compra.clinica).get();
    compra.clinica = clinica.data();
    return compra;
  }

  _formatData(dataString) {
    let separados = dataString.split("/");
    const ano = parseInt(separados[2], 10);
    const mes = parseInt(separados[1], 10) - 1;
    const dia = parseInt(separados[0], 10);
    const data = new Date(ano, mes, dia);
    return data;
  }

  async get(req, res) {
    try {
      let retorno = [];
      if (typeof req.query.cliente !== "undefined") {
        let compras = await this.Compras.where(
          "cliente",
          "==",
          Cliente.doc(req.query.cliente)
        ).get();
        let comprasDocs = compras.docs;
        comprasDocs.map(doc => {
          const compra = this._formatCompra(doc);
          retorno.push(compra);
        });
        res.send(retorno);
      } else if (typeof req.query.clinica !== 'undefined') {
        
        if (typeof req.currentUser.cpf != 'undefined') {
          
          const clinicaQuery = await Clinica.where('cnpj', '==', req.currentUser.cpf).limit(1).get();
          let comprasRetorno = [];
          const compras = await this.Compras.where('clinica', '==', clinicaQuery.docs[0].ref).orderBy('data_compra').get();
          
          comprasRetorno = await Promise.all(compras.docs.map(async (doc) => {
            let compra = this._formatCompra(doc);
            compra = await this._formatCliente(compra);
            compra = await this._formatClinica(compra);
            return compra;
          }));
          
          res.send(comprasRetorno);
       }
      } else { 
        let compras = await this.Compras.get();
        let comprasDocs = compras.docs;
        comprasDocs.map(doc => {
          const compra = this._formatCompra(doc);
          retorno.push(compra);
        });
        res.send(retorno);
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({ message: "erro" });
    }
  }

  async getById(req, res) {
    try {
      const id = req.params.id;
      const compraDoc = await this.Compras.doc(id).get();
      if (compraDoc.exists) {
        const compra = await this._formatFullCompra(compraDoc);
        res.send(compra);
      } else {
        res.status(404).send({ message: "Esta compra não existe!" });
      }
    } catch (err) {
      console.log(err);
      res.status(500).send({ message: "Erro" });
    }
  }

  async update(req, res) {
    try {
      const compra = await this.Compras.doc(req.params.id);
      const compraRef = await compra.get();
      if (compraRef.exists) {
        await compra.update(req.body);
        const updated = await compra.get();
        res.send(updated.data());
      }
    } catch(err) {
      console.log(err);
      res.sendStatus(500);
    }
  }

  async create(req, res) {
    try {
      const body = req.body;
      // const data = this._formatData(new Date());
      const timestamp = firebase.firestore.Timestamp.fromDate(new Date());
      const newCompra = {
        cliente: Cliente.doc(body.cliente),
        clinica: Clinica.doc(body.clinica),
        procedimento: body.procedimento,
        data_compra: timestamp,
        pago: false
      };
      const newCompraref = await this.Compras.add(newCompra);
      const doc = await newCompraref.get();
      let compra = doc.data();
      compra.id = doc.id;
      compra = await this._formatFullCompra(doc);
      res.send(compra)
    } catch (err) {
      console.log(err);
      res.status(500).send({ message: "Erro" });
    }
  }

  async delete(req, res) {
    try {
      const doc = await this.Compras.doc(req.params.id);
      doc.delete();
      res.status(204).send({ message: "Sucesso" });
    } catch (error) {
      console.log(error);
      res.status(500).send({ message: "Erro" });
    }
  }
}

export default ComprasController;
