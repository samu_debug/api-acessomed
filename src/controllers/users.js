import Hash from '../util/hash';
class UsersController {
  constructor(User) {
    this.Hash = new Hash();
    this.User = User;
  }

  checkAdmin(req, res){
    try{
      if (req.currentUser.type !== 'admin'){
        console.log('entro');
        res.status(401).json({message: 'Somente administradores'});
      }
    }catch(err){
      res.status(401).json({message: 'Somente administradores'});
    }
    
  }

  async get(req, res) {
    this.checkAdmin(req, res);
    try {
      const users = await this.User.get();
      let data = [];
      await users.forEach((doc) => {
        const user = {
          name: doc.data().name,
          username: doc.id,
          password: doc.data().password
        };
        data.push(user);
      });
      res.send(data);
    }catch(err){
      res.status(401).send(err)
    }
    
  
  }

  async create(req, res) {
    this.checkAdmin(req, res);
    try{
      
      const user = await this.User.where("username", "==", req.body.username).get();

      if(!user.empty){
        res.status(401).send({message: 'Este usuário já existe!'});
      }else{
        req.body.password = await this.Hash.hash(req.body.password);
        const newUser = await this.User.add({
          name: req.body.name,
          username: req.body.username,
          password: req.body.password,
          type: 'admin'
        })
        res.send({message: 'Sucesso'});
        
      }
      
      
    }catch(err){
      console.log(err);
      res.status(401).send({message: 'Erro!'});
    }
    
  }

  async getByid(req, res){
    this.checkAdmin(req, res);
    const user = await this.User.doc(req.params.id).get();
    const retorno = {
      username: user.id,
      name: user.data().name,
      password: user.data().password
    }
    res.send(retorno);
  }

  update(req, res) {
    this.checkAdmin(req, res);
    return this.User.findOneAndUpdate({ _id: req.params.id }, req.body)
      .then(() => res.sendStatus(200))
      .catch(err => res.status(422).send(err.message));
  }

  remove(req, res) {
    this.checkAdmin(req, res);
    return this.User.deleteOne({ _id: req.params.id })
      .then(() => res.sendStatus(204))
      .catch(err => res.status(400).send(err.message));
  }
}

export default UsersController;
