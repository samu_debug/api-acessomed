import * as firebase from 'firebase-admin';
const credentials = require('../config/acesso-med-firebase-adminsdk-zr8zp-d5b8557f9c.json');


if(firebase.apps.length == 0){
  firebase.initializeApp({
    credential: firebase.credential.cert(credentials),
    databaseURL: 'https://acesso-med.firebaseio.com/'
  });
}

const Cliente = firebase.firestore().collection('clientes');

export default Cliente;