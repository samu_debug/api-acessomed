import express from 'express';
import AgentesController from '../controllers/agentes';
import Agente from '../models/agente';

const router = express.Router();

const agentesController = new AgentesController(Agente);

router.get('/', (req, res) => {agentesController.get(req, res)});
router.get('/:id', (req,res) => {agentesController.getById(req, res)});
router.post('/', (req, res) => {agentesController.create(req, res)});
router.put('/:id', (req, res) => {agentesController.update(req, res)});
router.delete('/:id', (req, res) => {agentesController.delete(req, res)});

export default router;