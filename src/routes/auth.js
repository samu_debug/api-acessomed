import express from 'express';
import AuthController from '../controllers/auth';
import User from '../models/user';

const router = express.Router();

const authController = new AuthController(User);

router.post('/login', (req, res) => authController.login(req, res));
router.get('/get', (req, res) => authController.check(req, res));

export default router;