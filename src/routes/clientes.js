import express from 'express';
import ClienteController from '../controllers/clientes';
import Cliente from '../models/cliente';

const router = express.Router();

const clienteController = new ClienteController(Cliente);

router.get('/', (req, res) => clienteController.get(req, res));
router.get('/:id', (req, res) => clienteController.getById(req, res));
router.post('/', (req, res) => clienteController.create(req, res));
router.put('/:id', (req, res) => clienteController.update(req, res));
router.delete('/:id', (req, res) => clienteController.delete(req, res));

export default router;