import express from 'express';
import ClinicasController from '../controllers/clinicas';
import Clinica from '../models/clinica';

const router = express.Router();

const clinicasController = new ClinicasController(Clinica);

router.get('/', (req, res) => {clinicasController.get(req, res)});
router.get('/:id', (req, res) => {clinicasController.getById(req, res)});
router.post('/', (req, res) => {clinicasController.create(req, res)});
router.post('/:id/procedimentos', (req, res) => {clinicasController.createProcedimento(req, res)}); 
router.put('/:id', (req, res) => {clinicasController.update(req,res)});
router.delete('/:id', (req, res) => {clinicasController.delete(req, res)});

export default router;