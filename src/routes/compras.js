import express from 'express';
import ComprasController from '../controllers/compras';
import Compras from '../models/compras';

const router = express.Router();

const comprasController = new ComprasController(Compras);

router.get('/', (req, res) => comprasController.get(req, res));

router.get('/:id', (req, res) => comprasController.getById(req, res));
router.post('/', (req, res) => comprasController.create(req, res));
router.put('/:id', (req, res) => comprasController.update(req, res));
router.delete('/:id', (req, res) => comprasController.delete(req, res));

export default router;