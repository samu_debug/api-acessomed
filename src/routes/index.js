import express from 'express';
import usuariosRoute from './users';
import agentesRoute from './agentes';
import clinicasRoute from './clinicas';
import clientesRoute from './clientes';
import comprasRoute from './compras';
import auth from '../util/auth';
import authRoute from './auth';
const router = express.Router();

router.use('/auth', authRoute);
router.get('/', (req, res) => res.send({message: 'App Online!'}));
router.use((req, res, next) => {
    auth(req, res, next);
});
router.use('/users', usuariosRoute);
router.use('/agentes', agentesRoute);
router.use('/compras', comprasRoute);
router.use('/clinicas', clinicasRoute);
router.use('/clientes', clientesRoute);




export default router;
