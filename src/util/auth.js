import * as firebase from 'firebase-admin';
let auth = async (req, res, next) => {
    try{
        const token = req.header('Token');
        const decodedToken = await firebase.auth().verifyIdToken(token);
        req.currentUser = {
            cpf: decodedToken.uid,
            type: decodedToken.type,
            database_id: decodedToken.id
        };
        next();

    }catch(err){
        res.status(401).json({message: 'Token inválido'});
    }
    
}
export default auth;