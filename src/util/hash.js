import bcrypt from 'bcryptjs';
const SALT_WORK_FACTOR = 10;
class Hashing { 

    async hash(password){
        
        return bcrypt.hash(password, SALT_WORK_FACTOR);
    }

    async compare(candidate, password){
        return bcrypt.compareSync(candidate, password);
    }
}

export default Hashing;